package com.myepam.app;
public class Calculator{
    private double num1,num2;
    private char e;
    private double r;
    public Calculator(double num1, double num2,char e){
        this.num1 = num1;
        this.num2 = num2;
        this.e=e;
    }
    public void eval(){
        if(e == '+')
            r=  num1+num2;
        else if(e =='-')
            r= num1-num2;
        else if(e == '*')
            r= num1*num2;
        else if(e == '/')
            r= num1/num2;        
    }
 public void disp(){
        System.out.println("The output result is : "+r);
    }
}